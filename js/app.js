// Foundation JavaScript
// Documentation can be found at: http://foundation.zurb.com/docs
$(document).foundation();

/**
 * Gera um background em um caontainer definido
 * @param  {Object} options
 * @return {String} 
 */
$.fn.getDataThumb = function(options) {
    options = $.extend({
        bgClass: 'bg-cover'
    }, options || {});
    return this.each(function() {
        var th = $(this).data('thumb');
        if (th) {
            $(this).css('background-image', 'url(' + th + ')').addClass(options.bgClass);
        }
    });
};
$('.get-thumb').getDataThumb();

$.fn.easyTabs = function(options) {
    options = $.extend({} || {});
    return this.each(function(index, el) {
        var $t = $(this);
        $('*[data-tab-item]', this).addClass('easy-tab-item').hide().first().show();
        $('*[data-tab]', this).bind('click', function(event) {
            event.preventDefault();
            var v = $(this).data('tab');
            if (!$(this).hasClass('active')) {
                $(this).addClass('active').siblings('*[data-tab]').removeClass('active')
            } else {
                $(this).removeClass('active');
            }
            $('*[data-tab-item]', $t).each(function(index, el) {
                if ($(this).data('tab-item') == v) {
                    if (!$(this).hasClass('open')) {
                        $(this).addClass('open').slideDown('fast').siblings('*[data-tab-item]').removeClass('open').slideUp('fast');
                    } else {
                        $(this).removeClass('open').slideUp('fast');
                        p.removeClass('active');
                    }
                }
            });
        });
    });
};
$('#tabs-sidebar').easyTabs();

/**
 * Ativa class active e desativa para 
 * ancoras irmãs em uma lista
 */
function deactiveSiblingsInList(cont) {
	$('a',cont).on('click',function(e) {
		e.preventDefault();
		$(this).addClass('active')
		.parents('li')
		.siblings('li')
		.find('a')
		.removeClass('active');
	});
};
deactiveSiblingsInList('.tabs-choose');

/**
 * Efeitos no hover do menu principal
 * @return {void} muda cor da borda
 */
function menuColorsFx() {
	var currentColor,
		currentBorder;

	$('li','#main-menu').each(function() {
		if($(this).hasClass('current-menu-item')) {
			currentColor = $(this).css('borderColor');
		}
	})
	.hover(
		function() {
			currentBorder = $(this).css('borderColor');
			$(this).parents('.inline-list').css('borderColor', currentBorder);
		},
		function() {
			$(this).parents('.inline-list').css('borderColor', currentColor);
		}
	);
	$('ul','#main-menu').css('borderColor', currentColor);
};
menuColorsFx();

/**
 * Constroi um menu dropdown a partir de uma lista
 * @param  {String} id container do menu original
 * @param  {String} copyTo id/class do menu dropdown
 * @return {String}        opções do menu dropdown
 */
function selectMenuMobile(id,copyTo) {
	var mainMenu = $(id);
	$('li',mainMenu).each(function() {
		var anchor = $('a',this).attr('href'),
			txt    = $('a',this).text();
		$(copyTo).append('<option value="'+ anchor +'">'+ txt +'</option>');
	});
};
selectMenuMobile("#main-menu","select[name=\"mo-menu\"]");
selectMenuMobile(".top-menu","select[name=\"mo-menu\"]");