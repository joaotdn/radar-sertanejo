<!doctype html>
<html class="no-js" lang="en">
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Radar Sertanejo</title>
    <link href='http://fonts.googleapis.com/css?family=Patua+One|Open+Sans:400italic,400,700' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="style.css" />
    <script src="bower_components/modernizr/modernizr.js"></script>
  </head>
  <body>
    
    <!-- top-menu -->
    <nav class="small-12 left top-menu show-for-large-up" role="navigation">
      <div class="row">
        <div class="small-12 columns">
          <ul class="d-iblock right no-margin text-up inline-list">
            <li>
              <h3><a href="#">Quem somos</a></h3>
            </li>
            <li>
              <h3><a href="#">Anuncie</a></h3>
            </li>
            <li>
              <h3><a href="#">O que oferecemos</a></h3>
            </li>
            <li>
              <h3><a href="#">Eu repórter</a></h3>
            </li>
            <li>
              <h3><a href="#">Contato</a></h3>
            </li>
          </ul>
        </div> 
      </div><!-- / row -->   
    </nav>
    <!-- / top-menu -->
    
    <!-- wrapper -->
    <div id="wrapper" class="row">
      <!-- header -->
      <header id="header" class="small-12 left centered-v">
        <figure class="logo small-12 large-4 columns small-text-center large-text-left">
          <h1><a href="#" title="Página principal"><img src="images/logo.png" alt=""></a></h1>
          <div class="divide-20 hide-for-large-up"></div>
        </figure> <!-- / logo -->

        <figure class="small-12 large-8 columns small-text-center large-text-right">
          <a href="#"><img src="http://www.diariodosertao.com.br/img/banners/1/geral/20150408141728.gif" alt=""></a>
          <div class="divide-20"></div>
        </figure> <!-- / ads -->

        <nav id="main-menu" class="small-12 columns show-for-large-up">
          <ul class="inline-list text-up no-margin">
            <li class="li-marine current-menu-item">
              <h3><a href="#">Home</a></h3>
            </li>

            <li class="li-yellow">
              <h3><a href="#">Política</a></h3>
            </li>

            <li class="li-blue">
              <h3><a href="#">Paraíba</a></h3>
            </li>

            <li class="li-green">
              <h3><a href="#">Brasil</a></h3>
            </li>

            <li class="li-sky">
              <h3><a href="#">Policial</a></h3>
            </li>

            <li class="li-violet">
              <h3><a href="#">Cultura</a></h3>
            </li>

            <li class="li-orange">
              <h3><a href="#">Cidades</a></h3>
            </li>

            <li class="li-red">
              <h3><a href="#">Radar Flash</a></h3>
            </li>
          </ul>
        </nav>

        <nav id="mobile-menu" class="small-12 columns hide-for-large-up">
          <select name="mo-menu" class="small-12 left">
          </select>
        </nav>
      </header>
      <!-- / header -->

      <section id="content" class="small-12 left">
        <div class="divide-10 show-for-large-up"></div>
        
        <!-- features -->
        <nav id="features-slide" class="small-12 medium-6 columns">
          
          <div class="small-12 left cycle-slideshow ov-hidden"
          data-cycle-fx="scrollHorz" 
          data-cycle-timeout="6000"
          data-cycle-slides="> figure"
          data-next=".next-feature"
          data-prev=".prev-feature"
          data-cycle-swipe="true"
          data-cycle-swipe-fx="scrollHorz">
            <figure class="small-12 left rel panel-post">
              <a href="#" title="" class="rel full-height small-12 left d-block get-thumb" data-thumb="http://participacao.mj.gov.br/pensandoodireito/wp-content/uploads/2013/09/senado_federal.jpg">
              </a>
              <figcaption class="small-12 abs p-bottom p-left">
                <h3><a href="#">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Facilis, molestias.</a></h3>
              </figcaption>
              <h5 class="cat-flag bg-blue"><a href="#" title="">Paraíba</a></h5>
            </figure>

            <figure class="small-12 left rel panel-post">
              <a href="#" title="" class="rel full-height small-12 left d-block get-thumb" data-thumb="http://public.pps.org.br/img/global/midia/2267ec01041664ffbf7999d66cc0f6f11234391571bancada_reforma-politica.jpg">
              </a>
              <figcaption class="small-12 abs p-bottom p-left">
                <h3><a href="#">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Facilis, molestias.</a></h3>
              </figcaption>
              <h5 class="cat-flag bg-yellow"><a href="#" title="">Política</a></h5>
            </figure>

            <figure class="small-12 left rel panel-post">
              <a href="#" title="" class="rel full-height small-12 left d-block get-thumb" data-thumb="http://bbel.com.br/upload/post/6f1dd41d-97b4-422d-bfb5-9db694e1dba6.jpg">
              </a>
              <figcaption class="small-12 abs p-bottom p-left">
                <h3><a href="#">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Facilis, molestias.</a></h3>
              </figcaption>
              <h5 class="cat-flag bg-green"><a href="#" title="">Brasil</a></h5>
            </figure>
            
            <!-- navigation -->
            <a href="#" title="" class="nav-slide next-feature d-table text-center abs p-right show-for-medium-up">
              <span class="middle"><i class="icon-chevron-thin-right"></i></span>
            </a>

            <a href="#" title="" class="nav-slide prev-feature d-table text-center abs p-left show-for-medium-up">
              <span class="middle"><i class="icon-chevron-thin-left"></i></span>
            </a>
          </div>

          <div class="divide-20"></div>
        </nav> <!-- / features-slide -->
        
        <nav id="features-news" class="small-12 medium-6 left">
          <div class="small-12 medium-8 columns news-column-1">
            <figure class="small-12 left rel panel-post medium-block">
              <a href="#" title="" class="rel full-height small-12 left d-block get-thumb" data-thumb="http://info.abril.com.br/noticias/blogs/trending-blog/files/2013/06/Reforma-Pol%C3%ADtica-J%C3%A1-620x399.jpg"></a>

              <figcaption class="small-12 abs p-bottom p-left">
                <h3><a href="#">Lorem ipsum dolor sit amet, consectetur adipisicing elit.</a></h3>
              </figcaption>
              <h5 class="cat-flag bg-sky"><a href="#" title="">Policial</a></h5>
            </figure>

            <figure class="small-12 left rel panel-post medium-block">
              <a href="#" title="" class="rel full-height small-12 left d-block get-thumb" data-thumb="http://nilljunior.com.br/blog/wp-content/uploads/2015/02/reforma-polc3adtica-jc3a1.jpg"></a>

              <figcaption class="small-12 abs p-bottom p-left">
                <h3><a href="#">Lorem ipsum dolor sit amet, consectetur adipisicing elit.</a></h3>
              </figcaption>
              <h5 class="cat-flag bg-violet"><a href="#" title="">Cultura</a></h5>
            </figure>
          </div>

          <div class="small-12 medium-4 columns news-column-2">
            <div class="divide-20 show-for-small"></div>
            <figure class="small-12 left rel panel-post medium-block aside">
              <a href="#" title="" class="rel full-height small-12 left d-block get-thumb" data-thumb="http://www.cienciapolitica.ufes.br/sites/cienciapolitica.ufes.br/files/imagem/800px-Congresso_do_Brasil.jpg"></a>

              <figcaption class="small-12 abs p-bottom p-left">
                <h3><a href="#">Lorem ipsum dolor sit amet, consectetur adipisicing elit.</a></h3>
              </figcaption>
              <h5 class="cat-flag bg-yellow"><a href="#" title="">Política</a></h5>
            </figure>
          </div>
        </nav>
        <!-- / features -->

        <!-- ads -->
        <section class="small-12 left ads centered-v">
          <div class="divide-20 show-for-small"></div>
          <figure class="small-12 medium-4 columns">
            <a href="#"><img src="http://www.cassiusclay.com.au/images/s_hken_06.jpg" alt=""></a>
            <div class="divide-20"></div>
          </figure>

          <figure class="small-12 medium-4 columns">
            <a href="#"><img src="http://www.adpost.com/classifieds/upload/au/health_beauty/au_health_beauty.9253.1.jpg" alt=""></a>
            <div class="divide-20"></div>
          </figure>

          <figure class="small-12 medium-4 columns">
            <a href="#"><img src="http://1.bp.blogspot.com/-TUJlBp8nw4E/U0Qn9F-k3ZI/AAAAAAAAIUc/TYWZMtCqwIc/s1600/too-much-email-logo.jpg" alt=""></a>
            <div class="divide-20"></div>
          </figure>
        </section>
        <!-- / ads -->
        
        <!-- last-news -->
        <section id="last-news" class="small-12 large-8 left">
          <div class="small-12 left">
            <!-- post -->
            <div class="small-12 medium-6 columns item-post">
              <figure class="divide-10 rel panel-post medium-block">
                <a href="#" title="" class="rel full-height small-12 left d-block get-thumb" data-thumb="http://enpi-info.eu/img/news/T%20civil%20soci.jpg"></a>
                <h5 class="cat-flag bg-violet"><a href="#" title="">Cultura</a></h5>
              </figure>

              <article class="divide-20">
                <h6 class="text-up divide-10">
                  <time class="silver left" pubdate>12 de abril, 2015</time>
                  <span class="right"><a href="#" title="" class="post-violet"><i class="icon-bubble font-medium"></i> 3</a></span>
                </h6>

                <h3 class="small-12 left font-large">
                  <a href="#" title="" class="post-violet d-block small-12 left">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Deserunt, sed, unde, ex enim accusantium.</a>
                </h3>

                <p class="font-small no-margin show-for-large-up">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Facilis, earum, quidem, rerum nemo illo vel quas optio suscipi...</p>
              </article>
            </div>
            <!-- / post -->

            <!-- post -->
            <div class="small-12 medium-6 columns item-post">
              <figure class="divide-10 rel panel-post medium-block">
                <a href="#" title="" class="rel full-height small-12 left d-block get-thumb" data-thumb="https://www.lds.org/bc/content/ldsorg/content/images/relief-society-sisters-in-congo-P1008-01-0726alts2-1.jpg"></a>
                <h5 class="cat-flag bg-orange"><a href="#" title="">Cidades</a></h5>
              </figure>

              <article class="divide-20">
                <h6 class="text-up divide-10">
                  <time class="silver left" pubdate>12 de abril, 2015</time>
                  <span class="right"><a href="#" title="" class="post-orange"><i class="icon-bubble font-medium"></i> 3</a></span>
                </h6>

                <h3 class="small-12 left font-large">
                  <a href="#" title="" class="post-orange d-block small-12 left">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Deserunt, sed, unde, ex enim accusantium.</a>
                </h3>

                <p class="font-small no-margin show-for-large-up">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Facilis, earum, quidem, rerum nemo illo vel quas optio suscipi...</p>
              </article>
            </div>
            <!-- / post -->

            <!-- post -->
            <div class="small-12 medium-6 columns item-post">
              <figure class="divide-10 rel panel-post medium-block">
                <a href="#" title="" class="rel full-height small-12 left d-block get-thumb" data-thumb="http://www.portalnikkei.com.br/wp-content/uploads/2013/06/douglas-akimoto-japao.jpg"></a>
                <h5 class="cat-flag bg-green"><a href="#" title="">Brasil</a></h5>
              </figure>

              <article class="divide-20">
                <h6 class="text-up divide-10">
                  <time class="silver left" pubdate>12 de abril, 2015</time>
                  <span class="right"><a href="#" title="" class="post-green"><i class="icon-bubble font-medium"></i> 3</a></span>
                </h6>

                <h3 class="small-12 left font-large">
                  <a href="#" title="" class="post-green d-block small-12 left">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Deserunt, sed, unde, ex enim accusantium.</a>
                </h3>

                <p class="font-small no-margin show-for-large-up">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Facilis, earum, quidem, rerum nemo illo vel quas optio suscipi...</p>
              </article>
            </div>
            <!-- / post -->

            <!-- post -->
            <div class="small-12 medium-6 columns item-post">
              <figure class="divide-10 rel panel-post medium-block">
                <a href="#" title="" class="rel full-height small-12 left d-block get-thumb" data-thumb="http://i.ytimg.com/vi/Dt3w42JN1Is/maxresdefault.jpg"></a>
                <h5 class="cat-flag bg-blue"><a href="#" title="">Paraíba</a></h5>
              </figure>

              <article class="divide-20">
                <h6 class="text-up divide-10">
                  <time class="silver left" pubdate>12 de abril, 2015</time>
                  <span class="right"><a href="#" title="" class="post-blue"><i class="icon-bubble font-medium"></i> 3</a></span>
                </h6>

                <h3 class="small-12 left font-large">
                  <a href="#" title="" class="post-blue d-block small-12 left">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Deserunt, sed, unde, ex enim accusantium.</a>
                </h3>

                <p class="font-small no-margin show-for-large-up">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Facilis, earum, quidem, rerum nemo illo vel quas optio suscipi...</p>
              </article>
            </div>
            <!-- / post -->

            <!-- post -->
            <div class="small-12 medium-6 columns item-post">
              <figure class="divide-10 rel panel-post medium-block">
                <a href="#" title="" class="rel full-height small-12 left d-block get-thumb" data-thumb="http://www.arbache.com/blog/wp-content/uploads/2012/03/IMG_0813-1024x764.jpg"></a>
                <h5 class="cat-flag bg-red"><a href="#" title="">Radar Flash</a></h5>
              </figure>

              <article class="divide-20">
                <h6 class="text-up divide-10">
                  <time class="silver left" pubdate>12 de abril, 2015</time>
                  <span class="right"><a href="#" title="" class="post-red"><i class="icon-bubble font-medium"></i> 3</a></span>
                </h6>

                <h3 class="small-12 left font-large">
                  <a href="#" title="" class="post-red d-block small-12 left">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Deserunt, sed, unde, ex enim accusantium.</a>
                </h3>

                <p class="font-small no-margin show-for-large-up">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Facilis, earum, quidem, rerum nemo illo vel quas optio suscipi...</p>
              </article>
            </div>
            <!-- / post -->

            <!-- post -->
            <div class="small-12 medium-6 columns item-post">
              <figure class="divide-10 rel panel-post medium-block">
                <a href="#" title="" class="rel full-height small-12 left d-block get-thumb" data-thumb="http://www.sabercultural.com/template/registros/fotos/Exposicao-da-Sociedade-Brasileira-de-Belas-Artes-premiacaoFoto02.jpg"></a>
                <h5 class="cat-flag bg-orange"><a href="#" title="">Cidades</a></h5>
              </figure>

              <article class="divide-20">
                <h6 class="text-up divide-10">
                  <time class="silver left" pubdate>12 de abril, 2015</time>
                  <span class="right"><a href="#" title="" class="post-orange"><i class="icon-bubble font-medium"></i> 3</a></span>
                </h6>

                <h3 class="small-12 left font-large">
                  <a href="#" title="" class="post-orange d-block small-12 left">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Deserunt, sed, unde, ex enim accusantium.</a>
                </h3>

                <p class="font-small no-margin show-for-large-up">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Facilis, earum, quidem, rerum nemo illo vel quas optio suscipi...</p>
              </article>
            </div>
            <!-- / post -->
          </div>
          
          <!-- ads -->
          <figure class="small-12 columns centered-v">
            <a href="#"><img src="https://puretextualitypr.files.wordpress.com/2015/02/some-video.png" alt="" class="small-12"></a>
            <div class="divide-10"></div>
          </figure>
          <!-- / ads -->
          
          <!-- lista de noticias -->
          <nav class="small-12 left list-news">
            <!-- post -->
            <div class="divide-20 column item-post">
              <figure class="small-12 medium-4 left rel panel-post medium-block">
                <a href="#" title="" class="rel full-height small-12 left d-block get-thumb" data-thumb="http://www.bahianoticias.com.br/fotos/principal_noticias/141679/IMAGEM_NOTICIA_5.jpg"></a>
                <h5 class="cat-flag bg-sky"><a href="#" title="">Policial</a></h5>
              </figure>
              <article class="small-12 medium-8 left pl-20">
                <div class="divide-10 show-for-small-only"></div>
                <h6 class="text-up small-12 left">
                  <time class="silver left" pubdate>12 de abril, 2015</time>
                  <span class="right"><a href="#" title="" class="post-sky"><i class="icon-bubble font-medium"></i> 3</a></span>
                </h6>
                <h3 class="no-margin lh-small font-large">
                  <a href="#" title="" class="post-sky">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Animi, nemo, quae, facilis ipsa officia repellendus</a>
                </h3>
                <div class="divide-10 show-for-large-up"></div>
                <p class="font-small no-margin show-for-medium-up">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Facilis, earum, quidem, rerum nemo illo vel quas optio suscipi...</p>
              </article>
            </div>
            <!-- / post -->

            <!-- post -->
            <div class="divide-20 column item-post">
              <figure class="small-12 medium-4 left rel panel-post medium-block">
                <a href="#" title="" class="rel full-height small-12 left d-block get-thumb" data-thumb="http://www.cortocabeloepinto.com/wp-content/uploads/2009/04/noticia_maira.jpg"></a>
                <h5 class="cat-flag bg-violet"><a href="#" title="">Cultura</a></h5>
              </figure>
              <article class="small-12 medium-8 left pl-20">
                <div class="divide-10 show-for-small-only"></div>
                <h6 class="text-up small-12 left">
                  <time class="silver left" pubdate>12 de abril, 2015</time>
                  <span class="right"><a href="#" title="" class="post-violet"><i class="icon-bubble font-medium"></i> 3</a></span>
                </h6>
                <h3 class="no-margin lh-small font-large">
                  <a href="#" title="" class="post-violet">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Animi, nemo, quae, facilis ipsa officia repellendus</a>
                </h3>
                <div class="divide-10 show-for-large-up"></div>
                <p class="font-small no-margin show-for-medium-up">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Facilis, earum, quidem, rerum nemo illo vel quas optio suscipi...</p>
              </article>
            </div>
            <!-- / post -->

            <!-- post -->
            <div class="divide-20 column item-post">
              <figure class="small-12 medium-4 left rel panel-post medium-block">
                <a href="#" title="" class="rel full-height small-12 left d-block get-thumb" data-thumb="http://perlbal.hi-pi.com/blog-images/738980/gd/13055042873/NOTICIA-QUENTE-NO-MUNDO-DA-MODA-Modelo-cai-na-passarela-da-Semana-de-Moda-de-Nova-York-Acidente-aco.jpg"></a>
                <h5 class="cat-flag bg-red"><a href="#" title="">Radar Flash</a></h5>
              </figure>
              <article class="small-12 medium-8 left pl-20">
                <div class="divide-10 show-for-small-only"></div>
                <h6 class="text-up small-12 left">
                  <time class="silver left" pubdate>12 de abril, 2015</time>
                  <span class="right"><a href="#" title="" class="post-red"><i class="icon-bubble font-medium"></i> 3</a></span>
                </h6>
                <h3 class="no-margin lh-small font-large">
                  <a href="#" title="" class="post-red">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Animi, nemo, quae, facilis ipsa officia repellendus</a>
                </h3>
                <div class="divide-10 show-for-large-up"></div>
                <p class="font-small no-margin show-for-medium-up">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Facilis, earum, quidem, rerum nemo illo vel quas optio suscipi...</p>
              </article>
            </div>
            <!-- / post -->

            <!-- post -->
            <div class="divide-20 column item-post">
              <figure class="small-12 medium-4 left rel panel-post medium-block">
                <a href="#" title="" class="rel full-height small-12 left d-block get-thumb" data-thumb="http://www.lidercap.com.br/img/lidercap/noticias/noticia0.png"></a>
                <h5 class="cat-flag bg-yellow"><a href="#" title="">Política</a></h5>
              </figure>
              <article class="small-12 medium-8 left pl-20">
                <div class="divide-10 show-for-small-only"></div>
                <h6 class="text-up small-12 left">
                  <time class="silver left" pubdate>12 de abril, 2015</time>
                  <span class="right"><a href="#" title="" class="post-yellow"><i class="icon-bubble font-medium"></i> 3</a></span>
                </h6>
                <h3 class="no-margin lh-small font-large">
                  <a href="#" title="" class="post-yellow">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Animi, nemo, quae, facilis ipsa officia repellendus</a>
                </h3>
                <div class="divide-10 show-for-large-up"></div>
                <p class="font-small no-margin show-for-medium-up">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Facilis, earum, quidem, rerum nemo illo vel quas optio suscipi...</p>
              </article>
            </div>
            <!-- / post -->

            <!-- post -->
            <div class="divide-20 column item-post">
              <figure class="small-12 medium-4 left rel panel-post medium-block">
                <a href="#" title="" class="rel full-height small-12 left d-block get-thumb" data-thumb="http://www.sabercultural.com/template/registros/fotos/Exposicao-da-Sociedade-Brasileira-de-Belas-Artes-premiacaoFoto02.jpg"></a>
                <h5 class="cat-flag bg-blue"><a href="#" title="">Paraíba</a></h5>
              </figure>
              <article class="small-12 medium-8 left pl-20">
                <div class="divide-10 show-for-small-only"></div>
                <h6 class="text-up small-12 left">
                  <time class="silver left" pubdate>12 de abril, 2015</time>
                  <span class="right"><a href="#" title="" class="post-blue"><i class="icon-bubble font-medium"></i> 3</a></span>
                </h6>
                <h3 class="no-margin lh-small font-large">
                  <a href="#" title="" class="post-blue">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Animi, nemo, quae, facilis ipsa officia repellendus</a>
                </h3>
                <div class="divide-10 show-for-large-up"></div>
                <p class="font-small no-margin show-for-medium-up">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Facilis, earum, quidem, rerum nemo illo vel quas optio suscipi...</p>
              </article>
            </div>
            <!-- / post -->

            <!-- post -->
            <div class="divide-20 column item-post">
              <figure class="small-12 medium-4 left rel panel-post medium-block">
                <a href="#" title="" class="rel full-height small-12 left d-block get-thumb" data-thumb="http://www.valec.gov.br/imgNoticias/noticia0508_3b_fiol_800.jpg"></a>
                <h5 class="cat-flag bg-blue"><a href="#" title="">Paraíba</a></h5>
              </figure>
              <article class="small-12 medium-8 left pl-20">
                <div class="divide-10 show-for-small-only"></div>
                <h6 class="text-up small-12 left">
                  <time class="silver left" pubdate>12 de abril, 2015</time>
                  <span class="right"><a href="#" title="" class="post-blue"><i class="icon-bubble font-medium"></i> 3</a></span>
                </h6>
                <h3 class="no-margin lh-small font-large">
                  <a href="#" title="" class="post-blue">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Animi, nemo, quae, facilis ipsa officia repellendus</a>
                </h3>
                <div class="divide-10 show-for-large-up"></div>
                <p class="font-small no-margin show-for-medium-up">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Facilis, earum, quidem, rerum nemo illo vel quas optio suscipi...</p>
              </article>
            </div>
            <!-- / post -->
          </nav>
          <!-- / lista de noticias -->

          <!-- ads -->
          <div class="small-12 left centered-v">
            <figure class="small-12 medium-6 columns">
              <a href="#"><img src="http://www.minuto24.com/img/publi/ajalar.jpg" alt=""></a>
            </figure>

            <figure class="small-12 medium-6 columns">
              <a href="#"><img src="http://s1.dmcdn.net/H9iG1/380x100-hQj.jpg" alt=""></a>
            </figure>
          </div>
          <!-- / ads -->

        </section>
        <!-- / last-news -->

        <!-- sidebar -->
        <aside id="sidebar" class="small-12 large-4 columns">
          <section class="widget-group small-12 left">
            
            <!-- tabs -->
            <section id="tabs-sidebar" class="small-12 medium-6 large-12 left aside-widget">
              <ul class="inline-list tabs-choose text-up">
                <li><h6><a href="#" title="" class="active" data-tab="radar-flash">Radar Flash</a></h6></li>
                <li><h6><a href="#" title="" data-tab="cidades">Cidades</a></h6></li>
                <li><h6><a href="#" title="" data-tab="politica">Política</a></h6></li>
              </ul>

              <nav class="tabs-items small-12 left">
                
                <div class="small-12 left tab-item active" data-tab-item="radar-flash">
                  <ul class="no-bullet">
                    <li>
                      <article class="divide-10 post-sidebar">
                        <hgroup>
                          <h5 class="cat-flag-aside font-small text-up">
                            <a href="#" class="left d-block bg-red">Radar Flash</a>
                          </h5>
                          <h3 class="small-12 left font-medium d-block">
                            <a href="#" title="" class="post-red">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ipsa, vel, accusamus, animi adipisci velit.</a>
                          </h3>
                        </hgroup>
                        <figure>
                          <a href="#" class="d-block"><img src="http://www.newzimbabwe.com/news/images/news_mnews_bGershem-Pasi-Zimra-250.jpg" alt=""></a>
                        </figure>
                      </article>
                    </li>

                    <li>
                      <article class="divide-10 post-sidebar">
                        <hgroup>
                          <h5 class="cat-flag-aside font-small text-up">
                            <a href="#" class="left d-block bg-red">Radar Flash</a>
                          </h5>
                          <h3 class="small-12 left font-medium d-block">
                            <a href="#" title="" class="post-red">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ipsa, vel, accusamus, animi adipisci velit.</a>
                          </h3>
                        </hgroup>
                        <figure>
                          <a href="#" class="d-block"><img src="http://gforl.forl.org.br/Content/Noticia/imagem_destaques_home_236.jpg" alt=""></a>
                        </figure>
                      </article>
                    </li>

                    <li>
                      <article class="divide-10 post-sidebar">
                        <hgroup>
                          <h5 class="cat-flag-aside font-small text-up">
                            <a href="#" class="left d-block bg-red">Radar Flash</a>
                          </h5>
                          <h3 class="small-12 left font-medium d-block">
                            <a href="#" title="" class="post-red">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ipsa, vel, accusamus, animi adipisci velit.</a>
                          </h3>
                        </hgroup>
                        <figure>
                          <a href="#" class="d-block"><img src="http://www.pontonovo.ba.gov.br/imagens/noticias/thumbs/mini_d00ddff908.jpg" alt=""></a>
                        </figure>
                      </article>
                    </li>
                  </ul>
                </div>

                <div class="small-12 left tab-item active" data-tab-item="cidades">
                  <ul class="no-bullet">
                    <li>
                      <article class="divide-10 post-sidebar">
                        <hgroup>
                          <h5 class="cat-flag-aside font-small text-up">
                            <a href="#" class="left d-block bg-orange">Cidades</a>
                          </h5>
                          <h3 class="small-12 left font-medium d-block">
                            <a href="#" title="" class="post-orange">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ipsa, vel, accusamus, animi adipisci velit.</a>
                          </h3>
                        </hgroup>
                        <figure>
                          <a href="#" class="d-block"><img src="http://efpt.org.br/thumbs.php?w=80&h=90&imagem=images/noticias/66/DSCN6831.JPG" alt=""></a>
                        </figure>
                      </article>
                    </li>

                    <li>
                      <article class="divide-10 post-sidebar">
                        <hgroup>
                          <h5 class="cat-flag-aside font-small text-up">
                            <a href="#" class="left d-block bg-orange">Cidades</a>
                          </h5>
                          <h3 class="small-12 left font-medium d-block">
                            <a href="#" title="" class="post-orange">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ipsa, vel, accusamus, animi adipisci velit.</a>
                          </h3>
                        </hgroup>
                        <figure>
                          <a href="#" class="d-block"><img src="http://gforl.forl.org.br/Content/Noticia/imagem_destaques_home_236.jpg" alt=""></a>
                        </figure>
                      </article>
                    </li>

                    <li>
                      <article class="divide-10 post-sidebar">
                        <hgroup>
                          <h5 class="cat-flag-aside font-small text-up">
                            <a href="#" class="left d-block bg-orange">Cidades</a>
                          </h5>
                          <h3 class="small-12 left font-medium d-block">
                            <a href="#" title="" class="post-orange">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ipsa, vel, accusamus, animi adipisci velit.</a>
                          </h3>
                        </hgroup>
                        <figure>
                          <a href="#" class="d-block"><img src="http://portaljenipapo.com/thumbs.php?w=80&h=90&imagem=images/noticias/77/a1a1jh.jpg" alt=""></a>
                        </figure>
                      </article>
                    </li>
                  </ul>
                </div>

                <div class="small-12 left tab-item active" data-tab-item="politica">
                  <ul class="no-bullet">
                    <li>
                      <article class="divide-10 post-sidebar">
                        <hgroup>
                          <h5 class="cat-flag-aside font-small text-up">
                            <a href="#" class="left d-block bg-yellow">Política</a>
                          </h5>
                          <h3 class="small-12 left font-medium d-block">
                            <a href="#" title="" class="post-yellow">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ipsa, vel, accusamus, animi adipisci velit.</a>
                          </h3>
                        </hgroup>
                        <figure>
                          <a href="#" class="d-block"><img src="http://files.itnet.com.br/areavip/a28093029b42bf0d66ede841929fe8a2.jpg" alt=""></a>
                        </figure>
                      </article>
                    </li>

                    <li>
                      <article class="divide-10 post-sidebar">
                        <hgroup>
                          <h5 class="cat-flag-aside font-small text-up">
                            <a href="#" class="left d-block bg-yellow">Política</a>
                          </h5>
                          <h3 class="small-12 left font-medium d-block">
                            <a href="#" title="" class="post-yellow">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ipsa, vel, accusamus, animi adipisci velit.</a>
                          </h3>
                        </hgroup>
                        <figure>
                          <a href="#" class="d-block"><img src="http://portaljenipapo.com/thumbs.php?w=80&h=90&imagem=images/noticias/60/11124710_880087205415868_1645784454_n.jpg" alt=""></a>
                        </figure>
                      </article>
                    </li>

                    <li>
                      <article class="divide-10 post-sidebar">
                        <hgroup>
                          <h5 class="cat-flag-aside font-small text-up">
                            <a href="#" class="left d-block bg-yellow">Política</a>
                          </h5>
                          <h3 class="small-12 left font-medium d-block">
                            <a href="#" title="" class="post-yellow">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ipsa, vel, accusamus, animi adipisci velit.</a>
                          </h3>
                        </hgroup>
                        <figure>
                          <a href="#" class="d-block"><img src="http://www.portalagoranoticias.com.br/thumbs.php?w=80&h=90&imagem=images/noticias/9193/vereadores.JPG" alt=""></a>
                        </figure>
                      </article>
                    </li>
                  </ul>
                </div>

              </nav>
            </section>
            <!-- / tabs -->
              
            <!-- ads -->
            <figure class="small-12 medium-6 large-12 left aside-widget text-center">
              <a href="#">
                <img src="http://secure.theselfdefenseco.com/affiliate/displayImage.jsp?code=F503E41FEFC0890132BE0693E8051600" alt="">
              </a>
            </figure>
            <!-- / ads -->
            
            <div class="small-12 left show-for-medium-only"></div>

            <!-- ultimas noticias -->
            <section class="small-12 medium-6 large-12 left aside-widget">
              <header>
                <h5 class="text-up divide-20">Últimas notícias</h5>
              </header>

              <nav class="small-12 left">
                <div class="small-12 left tab-item">
                  <ul class="no-bullet">
                    <li>
                      <article class="divide-10 post-sidebar">
                        <hgroup>
                          <h5 class="cat-flag-aside font-small text-up">
                            <a href="#" class="left d-block bg-yellow">Política</a>
                          </h5>
                          <h3 class="small-12 left font-medium d-block">
                            <a href="#" title="" class="post-yellow">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ipsa, vel, accusamus, animi adipisci velit.</a>
                          </h3>
                        </hgroup>
                        <figure>
                          <a href="#" class="d-block"><img src="http://files.itnet.com.br/areavip/a28093029b42bf0d66ede841929fe8a2.jpg" alt=""></a>
                        </figure>
                      </article>
                    </li>

                    <li>
                      <article class="divide-10 post-sidebar">
                        <hgroup>
                          <h5 class="cat-flag-aside font-small text-up">
                            <a href="#" class="left d-block bg-blue">Paraíba</a>
                          </h5>
                          <h3 class="small-12 left font-medium d-block">
                            <a href="#" title="" class="post-blue">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ipsa, vel, accusamus, animi adipisci velit.</a>
                          </h3>
                        </hgroup>
                        <figure>
                          <a href="#" class="d-block"><img src="http://portaljenipapo.com/thumbs.php?w=80&h=90&imagem=images/noticias/60/11124710_880087205415868_1645784454_n.jpg" alt=""></a>
                        </figure>
                      </article>
                    </li>

                    <li>
                      <article class="divide-10 post-sidebar">
                        <hgroup>
                          <h5 class="cat-flag-aside font-small text-up">
                            <a href="#" class="left d-block bg-violet">Cultura</a>
                          </h5>
                          <h3 class="small-12 left font-medium d-block">
                            <a href="#" title="" class="post-violet">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ipsa, vel, accusamus, animi adipisci velit.</a>
                          </h3>
                        </hgroup>
                        <figure>
                          <a href="#" class="d-block"><img src="http://www.portalagoranoticias.com.br/thumbs.php?w=80&h=90&imagem=images/noticias/9193/vereadores.JPG" alt=""></a>
                        </figure>
                      </article>
                    </li>
                  </ul>
                </div>
              </nav>
            </section>
            <!-- / ultimas noticias -->

            <!-- colunistas -->
            <section class="small-12 medium-6 large-12 left aside-widget">
              <header>
                <h5 class="text-up divide-20">Colunistas</h5>
              </header>

              <nav class="small-12 left">
                <div class="small-12 left tab-item">
                  <ul class="no-bullet">
                    <li>
                      <article class="divide-10 post-sidebar">
                        <hgroup>
                          <h5 class="cat-flag-aside font-small text-up">
                            <a href="#" class="left d-block bg-primary">Joana Dark</a>
                          </h5>
                          <h3 class="small-12 left font-medium d-block">
                            <a href="#" title="" class="post-primary">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ipsa, vel, accusamus, animi adipisci velit.</a>
                          </h3>
                        </hgroup>
                        <figure>
                          <a href="#" class="d-block"><img src="http://4.bp.blogspot.com/-Zg4j74NuZcE/U3KtLVwwj4I/AAAAAAAAG1M/7WBcrkY9EX4/s1600/TR+thumb.jpg" alt=""></a>
                        </figure>
                      </article>
                    </li>

                    <li>
                      <article class="divide-10 post-sidebar">
                        <hgroup>
                          <h5 class="cat-flag-aside font-small text-up">
                            <a href="#" class="left d-block bg-primary">Renato Russo</a>
                          </h5>
                          <h3 class="small-12 left font-medium d-block">
                            <a href="#" title="" class="post-primary">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ipsa, vel, accusamus, animi adipisci velit.</a>
                          </h3>
                        </hgroup>
                        <figure>
                          <a href="#" class="d-block"><img src="http://oupeltglobal.files.wordpress.com/2010/03/tim-ward.jpg?w=80&h=90" alt=""></a>
                        </figure>
                      </article>
                    </li>

                    <li>
                      <article class="divide-10 post-sidebar">
                        <hgroup>
                          <h5 class="cat-flag-aside font-small text-up">
                            <a href="#" class="left d-block bg-primary">Chun Li</a>
                          </h5>
                          <h3 class="small-12 left font-medium d-block">
                            <a href="#" title="" class="post-primary">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ipsa, vel, accusamus, animi adipisci velit.</a>
                          </h3>
                        </hgroup>
                        <figure>
                          <a href="#" class="d-block"><img src="http://ecx.images-amazon.com/images/I/91I%2BIhRsVoL._SX80_.jpg" alt=""></a>
                        </figure>
                      </article>
                    </li>
                    
                    <li>
                      <article class="divide-10 post-sidebar">
                        <hgroup>
                          <h5 class="cat-flag-aside font-small text-up">
                            <a href="#" class="left d-block bg-primary">Chico Boarque</a>
                          </h5>
                          <h3 class="small-12 left font-medium d-block">
                            <a href="#" title="" class="post-primary">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ipsa, vel, accusamus, animi adipisci velit.</a>
                          </h3>
                        </hgroup>
                        <figure>
                          <a href="#" class="d-block"><img src="https://wondersofpakistan.files.wordpress.com/2012/05/salami20120407152427107.jpg" alt=""></a>
                        </figure>
                      </article>
                    </li>

                    <li>
                      <article class="divide-10 post-sidebar">
                        <hgroup>
                          <h5 class="cat-flag-aside font-small text-up">
                            <a href="#" class="left d-block bg-primary">Albert Einstein</a>
                          </h5>
                          <h3 class="small-12 left font-medium d-block">
                            <a href="#" title="" class="post-primary">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ipsa, vel, accusamus, animi adipisci velit.</a>
                          </h3>
                        </hgroup>
                        <figure>
                          <a href="#" class="d-block"><img src="http://www.festivaldecineglobal.org/drgff2011/www/images/paneles/juan_carlos_val.jpg" alt=""></a>
                        </figure>
                      </article>
                    </li>
                  </ul>
                </div>
              </nav>
            </section>
            <!-- / colunistas -->
                  
            <!-- tags -->
            <section class="small-12 medium-6 large-12 left aside-widget">
              <header>
                <h5 class="text-up divide-20">Tags</h5>
              </header>
            </section>
            <!-- / tags -->

            <!-- ads -->
            <figure class="small-12 medium-6 large-12 left aside-widget text-center">
              <a href="#">
                <img src="http://www.adsandfind.com/pageinfo/pageinfo_picture/idschool_comp_1.gif" alt="">
              </a>
            </figure>
            <!-- / ads -->

          </section>
        </aside>
        <!-- / sidebar -->
          
        <!-- footer-news -->
        <section class="footer-news small-12 left">
          <div class="divide-20"></div>

          <div class="small-12 medium-4 columns feature-post">
            <figure class="small-12 left rel panel-post">
              <a href="#" title="" class="rel full-height small-12 left d-block get-thumb" data-thumb="http://s2.glbimg.com/Q5Rr-Kan9_a0CyijkHxYilVOQ3k=/620x465/s.glbimg.com/jo/g1/f/original/2013/11/30/acidente_em_novo_lino_-_luzamir_carneiro.jpg">
              </a>
              <figcaption class="small-12 abs p-bottom p-left">
                <h3><a href="#">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Facilis, molestias.</a></h3>
              </figcaption>
              <h5 class="cat-flag bg-blue"><a href="#" title="">Paraíba</a></h5>
            </figure>
            <div class="divide-20"></div>
          </div>

          <div class="small-12 medium-4 columns feature-post">
            <figure class="small-12 left rel panel-post">
              <a href="#" title="" class="rel full-height small-12 left d-block get-thumb" data-thumb="http://gruporbs.clicrbs.com.br/files/2013/02/Equipe-do-jornal-A-Not%C3%ADcia.jpg">
              </a>
              <figcaption class="small-12 abs p-bottom p-left">
                <h3><a href="#">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Facilis, molestias.</a></h3>
              </figcaption>
              <h5 class="cat-flag bg-violet"><a href="#" title="">Cultura</a></h5>
            </figure>
            <div class="divide-20"></div>
          </div>

          <div class="small-12 medium-4 columns news-column-1">
            <figure class="small-12 left rel panel-post medium-block">
              <a href="#" title="" class="rel full-height small-12 left d-block get-thumb" data-thumb="http://info.abril.com.br/noticias/blogs/trending-blog/files/2013/06/Reforma-Pol%C3%ADtica-J%C3%A1-620x399.jpg"></a>

              <figcaption class="small-12 abs p-bottom p-left">
                <h3><a href="#">Lorem ipsum dolor sit amet, consectetur adipisicing elit.</a></h3>
              </figcaption>
              <h5 class="cat-flag bg-sky"><a href="#" title="">Policial</a></h5>
            </figure>

            <figure class="small-12 left rel panel-post medium-block">
              <a href="#" title="" class="rel full-height small-12 left d-block get-thumb" data-thumb="http://nilljunior.com.br/blog/wp-content/uploads/2015/02/reforma-polc3adtica-jc3a1.jpg"></a>

              <figcaption class="small-12 abs p-bottom p-left">
                <h3><a href="#">Lorem ipsum dolor sit amet, consectetur adipisicing elit.</a></h3>
              </figcaption>
              <h5 class="cat-flag bg-violet"><a href="#" title="">Cultura</a></h5>
            </figure>
          </div>
        </section>
        <!-- footer-news -->

      </section><!-- / content -->
    </div>
    <!-- / wrapper -->

    <!-- footer -->
    <div class="divide-20"></div>
    <footer id="footer" class="small-12 left bg-night section-block">
      <div class="row">
        <section class="footer-column small-12 large-4 columns">
          <header>
            <h6 class="white text-up">Quem somos</h6>
          </header>
          <p class="silver">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Molestias, magni, maxime, distinctio, voluptates ex adipisci ad qui ratione porro quaerat quibusdam nihil amet minus aperiam quia similique placeat deserunt aspernatur. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Reprehenderit, repellat, nobis, facilis praesentium odio unde rerum dolores incidunt id amet explicabo delectus. Accusantium, voluptatem, inventore ipsum mollitia incidunt nemo animi?</p>

          <h6 class="text-up"><a href="#" class="white">Saiba mais &raquo;</a></h6>
        </section>

        <section class="footer-column small-12 medium-4 columns show-for-large-up">
          <header>
            <h6 class="white text-up">Curta nossa fanpage</h6>
          </header>

          <div class="fb-page" data-href="https://www.facebook.com/radarsertanejo" data-width="100%" data-hide-cover="false" data-show-facepile="true" data-show-posts="false"><div class="fb-xfbml-parse-ignore"><blockquote cite="https://www.facebook.com/radarsertanejo"><a href="https://www.facebook.com/radarsertanejo">Banda Radar Sertanejo</a></blockquote></div></div>
        </section>

        <section class="footer-column small-12 medium-4 columns show-for-large-up">
          <a class="twitter-timeline" href="https://twitter.com/radar_sertanejo" data-widget-id="593305796039016448">Tweets by @radar_sertanejo</a>
          <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+"://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
        </section>
      </div>
    </footer>
    <section class="small-12 left centered-v bg-oil">
      <div class="row">
        <div class="small-12 columns">
          <h6 class="left text-up white no-margin">Copyright <?php echo date("Y"); ?></h6>
          
          <nav class="footer-menu show-for-large-up">
            <ul class="d-iblock right no-margin text-up inline-list">
              <li>
                <h6><a href="#">Quem somos</a></h6>
              </li>
              <li>
                <h6><a href="#">Anuncie</a></h6>
              </li>
              <li>
                <h6><a href="#">O que oferecemos</a></h6>
              </li>
              <li>
                <h6><a href="#">Eu repórter</a></h6>
              </li>
              <li>
                <h6><a href="#">Contato</a></h6>
              </li>
            </ul>
          </nav>
        </div>
      </div>
    </section>
    <!-- / footer -->
    
    <script src="bower_components/jquery/dist/jquery.min.js"></script>
    <script src="scripts.js"></script>
    <div id="fb-root"></div>
    <script>(function(d, s, id) {
      var js, fjs = d.getElementsByTagName(s)[0];
      if (d.getElementById(id)) return;
      js = d.createElement(s); js.id = id;
      js.src = "//connect.facebook.net/pt_BR/sdk.js#xfbml=1&version=v2.3&appId=311641142285753";
      fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));</script>
  </body>
</html>
