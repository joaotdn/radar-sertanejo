# Radar Setanejo

Desenvolvido com [Foccus](http://foccus.cc)

## Requisitos

  * Ruby 1.9+
  * [Node.js](http://nodejs.org)
  * [compass](http://compass-style.org/): `gem install compass`
  * [bower](http://bower.io): `npm install bower -g`
